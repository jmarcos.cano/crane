# from helpers import delivery
# from helpers import build
import helper_build
import helper_delivery
import click


@click.group()
def cli():
    pass


@cli.command("build")
@click.option('--f', '-f', metavar='KN', default=10,
              help='Speed in knots.')
def build(name):
    click.echo("Docker build {}".format(name))

@cli.command("package")
@click.argument('x', type=float)
def package(name):
    click.echo("Name {}".format(name))

cli.add_command(package)
cli.add_command(run)

if __name__ == '__main__':
    run()