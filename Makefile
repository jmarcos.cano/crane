TWINE_REPOSITORY_URL?=https://test.pypi.org/legacy/
init:
	@python3 -m pip  install -q -r requirements.txt

test:
	@docker run  -v ${PWD}:/test python:3-alpine /test/.build/local_test.sh crane
gitlab_test:
	@pip install -q virtualenv
	@virtualenv venv
	@. venv/bin/activate
	@pip install -q --editable .
	@crane

.PHONY: init test install

build dist install: init
	@python3 -m pip install -q --user --upgrade setuptools wheel
	@python3 -m pip install -q --user --upgrade twine
	@python3 setup.py sdist bdist_wheel

publish: dist build
	echo python3 -m twine upload --skip-existing -u ${TWINE_USERNAME} -p ${TWINE_PASSWORD} --repository-url ${TWINE_REPOSITORY_URL} dist/*

