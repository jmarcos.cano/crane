#import setuptools
from setuptools import setup, find_packages


with open("README.md", "r") as fh:
    readme = fh.read()
with open('LICENSE') as f:
    license = f.read()

setuptools.setup(
    name="crane",
    version="0.0.0",
    author="Marcos Cano",
    py_modules=['helpers'],
    author_email="marcos@kontinu.io",
    description="A small package to help to package, distribute artifacts based on Docker",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jmarcos.cano/mecano",
    packages=setuptools.find_packages(),
    license=license,
    install_requires=[
        'click',
        'colorama',
    ],
    entry_points='''
        [console_scripts]
        crane=crane:cli
    ''',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

)