#!/bin/bash


python3 -m pip install -q --user --upgrade setuptools wheel
python3 -m pip install -q --user --upgrade twine

python3 setup.py sdist bdist_wheel


python3 -m twine upload --skip-existing -u ${TWINE_USERNAME} -p ${TWINE_PASSWORD} --repository-url ${TWINE_REPOSITORY_URL:-https://test.pypi.org/legacy/} dist/*
