#!/bin/sh
cd /test
virtualenv venv  2>/dev/null || {
    pip install -q virtualenv
}

. venv/bin/activate

pip install -q --editable .


exec $@