# Basic logic

## crane.yml


# CI (.gitlab-ci & even local env)

- docker build? and grab the version of the Docker image? $(eval ${IMAGE_SUFFIX})
- generate octopus tar (.metadata)
- read .build/RELEASE_VERSION_SUFFIX.txt
- deploy REMOTE?

# RUNTIME (CD - bootstrap.sh)

- start
- stop
- restart
- rollback?
- logs
- config | show
- scale
- status
- version


### stack

### docker-compose

