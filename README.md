# Crane

A small utility that helps to package, distribute artifacts based on Docker images.

# Description


## Basic logic

### crane.yml


# CI (.gitlab-ci & even local env)

- docker build? and grab the version of the Docker image? $(eval ${IMAGE_SUFFIX})
- generate octopus tar (.metadata)
- read .build/RELEASE_VERSION_SUFFIX.txt
- deploy REMOTE?

# RUNTIME (CD - bootstrap.sh)

- start
- stop
- restart
- rollback?
- logs
- config | show
- scale
- status
- version


### stack

### docker-compose



# Links
- https://hackernoon.com/pip-install-abra-cadabra-or-python-packages-for-beginners-33a989834975